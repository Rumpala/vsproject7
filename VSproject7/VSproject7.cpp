﻿#include <iostream>

using namespace std;

class Stack
{
private:
    int last_index = 0;
    int max;
public:
    int* ptrarray;
    Stack(int size)
    {
        ptrarray = new int[size];
        max = size;
    }
    void push(int new_elem)
    {
        if (last_index >= 0 && last_index < max)
        {
            ptrarray[last_index] = new_elem;
            last_index++;
        };
    }
    int pop()
    {
        if (last_index > 0)
        {
            last_index--;
            return ptrarray[last_index];
        };
    }
    ~Stack()
    {
        delete[] ptrarray;
    }
};

int main()
{
    Stack s(10);
    s.push(4);
    std::cout << s.pop();

}